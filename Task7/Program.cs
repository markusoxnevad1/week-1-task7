﻿using System;
using System.Collections.Generic;



namespace Task7
{   //  Program that add every fibonacci number below 4000000
    class EvenFibonacciMaker
    {
        static void Main(string[] args)
        {
            int SumOfEven = 0;
            foreach (int number in FibonacciSequence())
            {
                if (number %2 == 0)
                {
                    SumOfEven += number;
                }
            }
            Console.WriteLine("The sum of every even fibonaccci number below 4000000 is: "+ SumOfEven);
        }
        //  Creates a list with every fibonacci number below 4000000
        public static List<int> FibonacciSequence()
        {
            List<int> numbers = new List<int>();
            for (int i = 0; Fibonacci(i) < 4000000; i++)
            {
                numbers.Add(Fibonacci(i));
            }
            return numbers;
        }
        //  Generates fibonacci numbers
        public static int Fibonacci(int n)
        {
            int a = 1;
            int b = 2;

            for (int i = 0; i<n; i++)
            {
                int temporary = a;
                a = b;
                b = temporary + b;
            }
            return a;
        }
    }
}
